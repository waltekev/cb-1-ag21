#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include "syntree.h"

const size_t SYNTREE_NODE_LIST_INIT_SIZE = 1;
const size_t SYNTREE_NODE_LIST_MAX_SIZE = SIZE_MAX;

const size_t NODEID_NODE_LIST_INIT_SIZE = 1;
const size_t NODEID_NODE_LIST_MAX_SIZE = SIZE_MAX;

size_t nextID = 0;

void exitWithError(Syntree *self, char* errMsg) {
    fprintf(stderr, errMsg);
    syntreeRelease(self);
    exit(-1);
}

int syntreeInit(Syntree *self) {
    self->nodeList = malloc(sizeof(SyntreeNodeID *) * SYNTREE_NODE_LIST_INIT_SIZE);
    if(self->nodeList == NULL) {
        fprintf(stderr, "Error: Cant allocate memory for self->nodeList.\n");
        return -1;
    }

    self->size = SYNTREE_NODE_LIST_INIT_SIZE;
    self->count = 0;

    return 0;
}

void syntreeRelease(Syntree *self) {
    //printf("Count: %u\n", self->count);
    
    for(size_t i = 0; i < self->count; i++) {
        free(self->nodeList[i]->nodeList);
        free(self->nodeList[i]);
    }

    free(self->nodeList);
}

void syntreeAddNode(Syntree *self, SyntreeNodeID *node) {
    if(self->count >= self->size) {
        //Resize self->nodeList
        if(self->count == SYNTREE_NODE_LIST_MAX_SIZE) 
            exitWithError(self, "Error: Syntree->nodeList has reached max size.\n");

        size_t newSize;
        if(self->size >= SYNTREE_NODE_LIST_MAX_SIZE / 2) {
            newSize = SYNTREE_NODE_LIST_MAX_SIZE;
        }else {
            newSize = self->size * 2;
        }

        self->nodeList = realloc(self->nodeList, sizeof(SyntreeNodeID *) * newSize);
        if(self->nodeList == NULL)
            exitWithError(self, "Error: Cant reallocate memory for Syntree->nodeList.\n");
        
        self->size = newSize;
        syntreeAddNode(self, node);
    }else {
        //Add node
        self->nodeList[self->count] = node;
        self->count += 1;
    }
}

void syntreeNodeAddNode(Syntree *tree, SyntreeNodeID *self, SyntreeNodeID *addID, int prepend) {
    if(self->count >= self->size) {
        //Resize self->nodeList
        if(self->count == NODEID_NODE_LIST_MAX_SIZE) 
            exitWithError(tree, "Error: SyntreeNodeID->nodeList has reached max size.\n");

        size_t newSize;
        if(self->size >= NODEID_NODE_LIST_MAX_SIZE / 2) {
            newSize = NODEID_NODE_LIST_MAX_SIZE;
        }else {
            newSize = self->size * 2;
        }
        
        self->nodeList  = realloc(self->nodeList, sizeof(SyntreeNodeID) * newSize);
        if(self->nodeList == NULL)
            exitWithError(tree, "Error: Cant reallocate memory for SyntreeNodeID->nodeList.\n");

        self->size = newSize;
        syntreeNodeAddNode(tree, self, addID, prepend);
    }else {
        if(prepend == 0) {
            //Append Node
            self->nodeList[self->count] = *addID;
            self->count += 1;
        }else {
            //Prepend Node
            for(size_t i = self->count - 1; i >= 0; i--) {
                self->nodeList[i+1] = self->nodeList[i];

                if(i == 0) {
                    self->nodeList[0] = *addID;
                    self->count += 1;
                    break;
                }
            }
        }
    }
}

SyntreeNodeID syntreeNodeNumber(Syntree *self, int number) {
    SyntreeNodeID *newNode = malloc(sizeof(SyntreeNodeID));
    if(newNode == NULL) {
        exitWithError(self, "Error: Cant allocate memory for newNode.\n");
    }

    newNode->isListNode = 0;
    newNode->id = nextID;
    nextID += 1;

    //LiteralNode Variables
    newNode->value = number;

    //ListNode Variables
    newNode->nodeList = malloc(sizeof(SyntreeNodeID) * 1);
    if(newNode->nodeList == NULL) {
        exitWithError(self, "Error: Cant allocate memory for newNode->nodeList.\n");
    }

    newNode->size = 1;
    newNode->count = 0;

    syntreeAddNode(self, newNode);
    return *newNode;
}

SyntreeNodeID* createNewListNode(Syntree *self) {
    SyntreeNodeID *newListNode = malloc(sizeof(SyntreeNodeID));
    if(newListNode == NULL) {
        exitWithError(self, "Error: Cant allocate memory for newListNode.\n");
    }

    newListNode->isListNode = 1;
    newListNode->id = nextID;
    nextID += 1;

    //LiteralNode Variables
    newListNode->value = 0; //not used because isListNode = 1

    //ListNode Variables
    newListNode->nodeList = malloc(sizeof(SyntreeNodeID) * NODEID_NODE_LIST_INIT_SIZE);
    if(newListNode->nodeList == NULL) {
        exitWithError(self, "Error: Cant allocate memory for newNode->nodeList.\n");
    }

    newListNode->size = NODEID_NODE_LIST_INIT_SIZE;
    newListNode->count = 0;

    syntreeAddNode(self, newListNode);
    return newListNode;
}

SyntreeNodeID syntreeNodeTag(Syntree *self, SyntreeNodeID id) {
    SyntreeNodeID *newListNode = createNewListNode(self);

    //Add id to newListNode->nodeList
    syntreeNodeAddNode(self, newListNode, &id, 0);

    return *newListNode;
}

SyntreeNodeID syntreeNodePair(Syntree *self, SyntreeNodeID id1, SyntreeNodeID id2) {
    SyntreeNodeID *newListNode = createNewListNode(self);

    syntreeNodeAddNode(self, newListNode, &id1, 0);
    syntreeNodeAddNode(self, newListNode, &id2, 0);

    return *newListNode;
}

SyntreeNodeID syntreeNodeAppend(Syntree *self, SyntreeNodeID list, SyntreeNodeID elem) {
    size_t listID = list.id;
    SyntreeNodeID *listNode = self->nodeList[listID];

    syntreeNodeAddNode(self, listNode, &elem, 0);
    return *listNode;
}

SyntreeNodeID syntreeNodePrepend(Syntree *self, SyntreeNodeID elem, SyntreeNodeID list) {
    size_t listID = list.id;
    SyntreeNodeID *listNode = self->nodeList[listID];

    syntreeNodeAddNode(self, listNode, &elem, 1);
    return *listNode;
}

void syntreePrintListNode(SyntreeNodeID root) {
    char* start = "{";
    char* end = "}";

    printf(start);

    for(size_t i = 0; i < root.count; i++) {
        if(root.nodeList[i].isListNode == 1) {
            syntreePrintListNode(root.nodeList[i]);
        }else {
            printf("(%d)", root.nodeList[i].value);
        }
    }
    
    printf(end);
}

void syntreePrint(const Syntree *self, SyntreeNodeID root) {
    if(root.isListNode == 1) {
        syntreePrintListNode(root);
        printf("\n");
    }else {
        printf("(%d)\n", root.value);
    }
}
