#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#include "stack.h"

const size_t STACK_INIT_SIZE = 1;
const size_t STACK_MAX_SIZE = SIZE_MAX;

void exitWithError(const IntStack *self, char *errMsg) {
    fprintf(stderr, errMsg);
    stackRelease((IntStack *)self);
    exit(-1);
}

int stackInit(IntStack *self) {
    self->stackPtr = malloc(sizeof(int) * STACK_INIT_SIZE);
    if(self->stackPtr == NULL) {
        fprintf(stderr, "Error: Memory allocation for stackValues failed.\n");
        return -1;
    }

    self->size = STACK_INIT_SIZE;
    self->count = 0;
    
    //printf("Size: %u Count: %u\n", self->size, self->count);
    //printf("stackInit finished.\n");

    return 0;
}

void stackRelease(IntStack *self) {
    free(self->stackPtr);
}

void stackPush(IntStack *self, int i) {
    //printf("Push %i - Current Size: %u Current Count: %u\n", i, self->size, self->count);

    //check if stack is full
    if(self->count >= self->size) {
        //stack is full -> increase size
        
        //printf("Stack is full - Size: %u Count: %u\n", self->size, self->count);

        //check if stack has reached max size
        if(self->size == STACK_MAX_SIZE)
            exitWithError(self, "Error: Stack reached STACK_MAX_SIZE.\n");

        //Calculate new stack size
        size_t newSize;
        if(self->size >= STACK_MAX_SIZE / 2) {
            newSize = STACK_MAX_SIZE;
        }else {
            newSize = self->size * 2;
        }

        //realloc memory for stack with new size
        self->stackPtr = realloc(self->stackPtr, sizeof(int) * newSize);
        if(self->stackPtr == NULL) {
            exitWithError(self, "Error: Memory allocation for stackValues failed.\n");
            return;
        }

        self->size = newSize;
        stackPush(self, i);
    }else {
        //stack has enough space left -> add i to stack
        self->stackPtr[self->count] = i;
        self->count += 1;
    }
}

int stackIsEmpty(const IntStack *self) {
    if(self->count == 0)
        return -1;

    return 0;
}

int stackTop(const IntStack *self) {
    if(stackIsEmpty(self) != 0) 
        exitWithError(self, "Error: Stack is empty, cant return top element.\n");

    return self->stackPtr[self->count - 1];
}

int stackPop(IntStack *self) {
    int top = stackTop(self);
    self->count -= 1;

    return top;
}

void stackPrint(const IntStack *self) {
    if(stackIsEmpty(self) == 0 ) {
        printf("--- print stack start ---\n");
        for(size_t i = self->count - 1; i >= 0; i--) {
            printf("%i, ", self->stackPtr[i]);

            if(i == 0) 
                break;
        }
        printf("\n--- print stack end ---\n");
    }
}